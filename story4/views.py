from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def profile(request):
    return render(request, 'Profile.html')

def hobbies(request):
    return render(request, 'hobbies.html')

def story1(request):
    return render(request, 'story-1.html')

def comingsoon(request):
    return render(request, 'comingsoon.html')

def navmenu(request):
    return render(request, 'navmenu.html')
