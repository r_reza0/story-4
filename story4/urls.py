from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.index, name='index'),
    path('Profile/', views.profile, name='Profile'),
    path('hobbies/', views.hobbies , name = 'hobbies'),
    path('comingsoon/', views.comingsoon, name = 'comingsoon'),
    path('story-1/', views.story1, name = 'story1'),
    path('navmenu/', views.navmenu, name = 'navmenu')
]