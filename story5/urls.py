from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.jadwal, name='jadwal'),
    path('<int:pk>/', views.delete, name = 'erase'),
    path('detail/<int:pk>',views.detail, name = "detail")
]