
from django import forms

class matakuliah(forms.Form):
       fields = ["namamatkul", "dosen","sks","deskripsi","tahun","ruangan"]
       error_messages = { "required": "masih kosong"}
       input_nama = {"type": "text", "placeholder":"nama matkul"}
       input_dosen = {"type": "text", "placeholder":"nama dosen"}
       input_sks = {"type": "Integer", "placeholder":"jumlah sks"}
       input_desc = {"type": "text", "placeholder":"deskripsi matkul"}
       input_tahun = {"type": "text", "placeholder":"tahun ajar"}
       input_kelas = {"type": "text", "placeholder":"ruang kelas"}
       
       namamatkul = forms.CharField(required = True,max_length=200, widget = forms.TextInput(attrs=input_nama))
       dosen = forms.CharField(required = True ,max_length = 200,widget = forms.TextInput(attrs=input_dosen))
       sks = forms.IntegerField(required = True, widget = forms.TextInput(attrs=input_sks))
       deskripsi = forms.CharField(required = True,max_length =1000,widget = forms.TextInput(attrs=input_desc))
       sem_tahun = forms.CharField(required = True, max_length = 200, widget = forms.TextInput(attrs=input_tahun))
       ruang_kelas = forms.CharField(required = True,max_length = 200, widget = forms.TextInput(attrs=input_kelas))
