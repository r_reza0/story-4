from django.shortcuts import redirect, render
from . import forms,models 

def jadwal(request):
    if request.method == "POST":
        formulir = forms.matakuliah(request.POST)
        if formulir.is_valid():
            subjects = models.matkul()
            subjects.nama = formulir.cleaned_data["namamatkul"]
            subjects.dosen = formulir.cleaned_data["dosen"]
            subjects.sks = formulir.cleaned_data["sks"]
            subjects.deskripsi = formulir.cleaned_data["deskripsi"]
            subjects.sem_tahun = formulir.cleaned_data["sem_tahun"]
            subjects.ruang_kelas = formulir.cleaned_data["ruang_kelas"]
            subjects.save()
        return redirect("/story5")
    else:
        form = forms.matakuliah()
        jadwal = models.matkul.objects.all()
        form_dict = {'formulir' : form,'jadwal' : jadwal}
    return render(request, 'jadwal.html', form_dict)

def detail(request, pk):
    jadwal = models.matkul.objects.get(id=pk)
    details ={'jadwal': jadwal}
    return render(request, 'detail.html',details)


def delete(request,pk):
    if request.method == "POST":
        formulir = forms.matakuliah(request.POST)
        if formulir.is_valid():
            subjects = models.matkul()
            subjects.nama = formulir.cleaned_data["namamatkul"]
            subjects.dosen = formulir.cleaned_data["dosen"]
            subjects.sks = formulir.cleaned_data["sks"]
            subjects.deskripsi = formulir.cleaned_data["deskripsi"]
            subjects.sem_tahun = formulir.cleaned_data["sem_tahun"]
            subjects.ruang_kelas = formulir.cleaned_data["ruang_kelas"]
            subjects.save()
        return redirect("/story5")
    else:
        models.matkul.objects.filter(pk = pk).delete()
        form = forms.matakuliah()
        jadwal = models.matkul.objects.all()
        form_dict = {'formulir' : form,'jadwal' : jadwal}
    return render(request, 'jadwal.html', form_dict)



