from django.db import models

# Create your models here.

class matkul(models.Model):
    nama = models.CharField(max_length = 200)
    dosen = models.CharField(max_length = 200)
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length =1000)
    sem_tahun = models.CharField(max_length = 200)
    ruang_kelas = models.CharField(max_length = 200)
